Does it make sense to think of a link between programming standards and more
traditional practices of writing, reading, publishing? There isn't a `W3C
publishing house', and most of these standards are simply HTML documents that
were collectively written. Still, some standards will leave the door open for
a lot of technical books. But beyond all the manuals, there's a dialogue with
standards that may start with a software implementation, and that will be
followed by lots of things that the implementors may or may not have imagined.
And some of these things will be books :-)

I'm trying to think of SVGOpen as a place where I could enrich my toolset and
workflows. How far can I take this idea? I guess that the discovery of an
exciting technology is partly a matter of serendipity, and there's the chance
that attending SVGOpen could be a step in that direction. If anything, I'm
wondering about the process of approaching a technology that's new to me. Did
I come back with a snippet that I could put to use soon? Or will my use of SVG
be totally unrelated to what I saw at the conference?

It's not a conference that's focused on free software. There was variety.
Under the umbrella of SVG there seems to be a place for the `advanced' demos
of a W3C working group, and for a workshop on an obscure technology like SVG
Fonts...
